# Game Of Life with VueJS

## Author
Amine Haddab

## Description
This project implements Conway's Game of Life using Vue.js. The Game of Life is a cellular automaton devised by the British mathematician John Horton Conway in 1970. It is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. One interacts with the Game of Life by creating an initial configuration and observing how it evolves.

## Installation
To get started with the project, follow these steps:

1. Clone the repository:
```bash
git clone https://gitlab.com/aminehaddab/game-of-life-with-vuejs.git
cd game-of-life-with-vuejs
```

2. Install project dependencies:
```bash
npm install
```

3. Run the project:
```bash
npm run dev
```